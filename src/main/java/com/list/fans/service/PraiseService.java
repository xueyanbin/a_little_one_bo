package com.list.fans.service;

import com.list.fans.model.Praise;

import java.util.List;

public interface PraiseService {
    /**
     * 点赞
     * @param praise
     */
    void insertPraiseByPost(Praise praise);

    /**
     * 取消点赞
     * @param praise
     */
    void deletePraiseByPost(Praise praise);

    /**
     * 根据用户id查询用户点赞过的帖子
     * @param userid
     * @return
     */
    public List<Praise> selectpraisepost(String userid);
}
