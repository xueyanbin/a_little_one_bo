package com.list.fans.service;

import com.list.fans.model.Attention;

import java.util.List;

public interface AttentionService {
    /**
     * 查询我的关注：我关注的用户
     * @param userId
     * @return
     */
    List<Attention> selectAttentionListByUserIdWithUsers(String userId);

    /**
     * 添加我的关注
     * @param attention
     */
    void insertAttentionUser (Attention attention);

    /**
     * 删除我的关注
     * @param attentionUserID
     * @param attentionPassiveUserId
     */
    void deletetAttentionUser(String attentionUserID,String attentionPassiveUserId);
}
