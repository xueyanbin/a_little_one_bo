package com.list.fans.service.impl;

import com.list.fans.mapper.TranspondMapper;
import com.list.fans.model.Transpond;
import com.list.fans.service.TranspondService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TranspondServiceImpl implements TranspondService {
    @Autowired
    TranspondMapper transpondMapper;

    /**
     * 插入转发
     * @param transpond
     */
    @Override
    public void insertTrans(Transpond transpond){
        transpondMapper.insertTrans(transpond);
    }

    /**
     * 删除转发
     * @param transpondId
     */
    @Override
    public void deleteTrans(String transpondId) {
        transpondMapper.deleteTrans(transpondId);
    }

    /**
     * 点赞
     * @param transpondId
     */
    @Override
    public  void updateTransPraiseUp(String transpondId){
        transpondMapper.updateTransPraiseUp(transpondId);
    }

    /**
     * 点赞
     * @param transpondId
     */
    @Override
    public  void updateTransPraiseDown(String transpondId){
        transpondMapper.updateTransPraiseDown(transpondId);
    }

    /**
     * 转发数+1
     */
   public void updateTransCountUp(String transpondId){
        transpondMapper.updateTransCountUp(transpondId);
   }

    /**
     * 转发数-1
     */
    public void deleteTransCountDown(String transpondId){
        transpondMapper.deleteTransCountDown(transpondId);
    }
}
