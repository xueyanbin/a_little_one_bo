package com.list.fans.service.impl;

import com.list.fans.mapper.CollectMapper;
import com.list.fans.model.Collect;
import com.list.fans.service.CollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectServiceImpl implements CollectService {
    @Autowired
    CollectMapper collectMapper;

    /**
     * 加入收藏
     */
    @Override
    public void insertCollect(Collect collect){
        collectMapper.insertCollect(collect);
    }

    /**
     * 删除收藏
     */
    @Override
    public  void deleteCollect(String collectId){
        collectMapper.deleteCollect(collectId);
    }

    /**
     * 根据用户id查询收藏
     * @param userid
     * @return
     */
    @Override
    public List<Collect> selectcollectpost(String userid) {
        return collectMapper.selectcollectpost(userid);
    }
}
