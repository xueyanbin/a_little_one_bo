package com.list.fans.service.impl;

import com.list.fans.mapper.PraiseMapper;
import com.list.fans.model.Praise;
import com.list.fans.service.PraiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PraiseServiceImpl implements PraiseService {
    @Autowired
    PraiseMapper praiseMapper;

    /**
     * 点赞
     * @param praise
     */
    public  void insertPraiseByPost(Praise praise){
         praiseMapper.insertPraiseByPost(praise);
    }

    /**
     * 取消点赞
     * @param praise
     */
    public void deletePraiseByPost(Praise praise){
        praiseMapper.deletePraiseByPost(praise);
    }

    /**
     * 查询用户点赞过的帖子
     * @param userid
     * @return
     */
    @Override
    public List<Praise> selectpraisepost(String userid) {
        return praiseMapper.selectpraisepost(userid);
    }
}
