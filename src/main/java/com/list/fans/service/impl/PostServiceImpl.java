package com.list.fans.service.impl;

import com.list.fans.mapper.PostMapper;
import com.list.fans.model.Post;
import com.list.fans.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    PostMapper postMapper;

    /**
     * 查询所有帖子
     * @return
     */
    @Override
    public List<Post> postSelectAll() {return postMapper.postSelectAll();}

    /**
     * 插入帖子
     * @param post
     * @return
     */
    @Override
    public void insertPost(Post post) {
         postMapper.insertPost(post);
    }
    /**
     * 删除帖子
     * 把帖子的status改0
     * @param postId
     */
    @Override
    public void deletePost(String postId){
        postMapper.deletePost(postId);
    }

    /**
     * 点赞
     * 更新POST_LIKE_COUNT+1
     * @param postId
     */
    @Override
    public void updatePostPraiseUp(String postId){
        postMapper.updatePostPraiseUp(postId);
    }

    /**
     * 取消赞
     * 更新POST_LIKE_COUNT-1
     * @param postId
     */
    @Override
    public void updatePostPraiseDown(String postId){
        postMapper.updatePostPraiseDown(postId);
    }

    /**
     * 转发
     */
    @Override
    public void updatePostTranUp(String postId) {
        postMapper.updatePostTranUp(postId);
    }

    /**
     * 删除转发-1
     */
    @Override
    public void  deletePostTranDown(String postId){
        postMapper.deletePostTranDown(postId);
    }


    /**
     * 根据帖子内容模糊搜索
     * @param posttent
     * @return
     */
    @Override
    public List<Post> selectPostmohu(String posttent) {
        return postMapper.selectPostmohu(posttent);
    }

    /**
     * 查看热门帖子
     * @return
     */
    @Override
    public List<Post> selecthotpost() {
        return postMapper.selecthotpost();
    }
    /**
     * 通过用户Id查询该用户发表的所有博客 默认降序
     * @return
     */
    @Override
    public List<Post> postSelectListByUserId(String userId) {
        return postMapper.postSelectListByUserId(userId);
    }
}
