package com.list.fans.service;

import com.list.fans.model.Collect;

import java.util.List;

public interface CollectService {
    /**
     * 加入收藏
     */
    void insertCollect(Collect collect);

    /**
     * 删除收藏
     */
    void deleteCollect(String collectId);

    /**
     * 根据用户id查询用户收藏
     * @param userid
     * @return
     */
    public List<Collect> selectcollectpost(String userid);
}
