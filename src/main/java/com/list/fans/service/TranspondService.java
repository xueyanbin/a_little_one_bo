package com.list.fans.service;

import com.list.fans.model.Transpond;

public interface TranspondService {
    /**
     * 插入转发
     * @param transpond
     */
    void insertTrans(Transpond transpond);

    /**
     * 删除转发
     * @param transpondId
     */
    void deleteTrans(String transpondId);
    /**
     * 点赞
     */
    void updateTransPraiseUp(String transpondId);
    /**
     * 取消点赞
     */
    void updateTransPraiseDown(String transpondId);

    /**
     * 转发数+1
     */
    void updateTransCountUp(String transpondId);

    /**
     * 转发数-1
     */
    void deleteTransCountDown(String transpondId);
}
