package com.list.fans.service;

import com.list.fans.model.Photo;

import java.util.List;

public interface PhotoService {
    /**
     * 根据用户id查询用户相册
     * @param userid
     * @return
     */
    public List<Photo> selectPhoto(String userid);

    /**
     * 插入图片
     * @param photo
     */
    void insertPhoto(Photo photo);
}
