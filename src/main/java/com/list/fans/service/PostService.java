package com.list.fans.service;

import com.list.fans.model.Post;

import java.util.List;

public interface PostService {
    /**
     * 查询所有帖子
     * @return
     */
    List<Post> postSelectAll();

    /**
     * 插入帖子
     * @param post
     * @return
     */
    void insertPost(Post post);

    /**
     * 删除帖子
     * 把帖子的status改0
     * @param postId
     */
    void deletePost(String postId);

    /**
     * 点赞
     * 更新POST_LIKE_COUNT+1
     * @param postId
     */
    void updatePostPraiseUp(String postId);

    /**
     * 取消赞
     * 更新POST_LIKE_COUNT-1
     * @param postId
     */
    void updatePostPraiseDown(String postId);

    /**
     * 转发
     */
    void updatePostTranUp(String postId);

    /**
     * 删除转发-1
     * @param postId
     */
    void  deletePostTranDown(String postId);

    /**
     * 根据帖子内容模糊搜索
     * @param posttent
     * @return
     */
    public  List<Post> selectPostmohu(String posttent);

    /**
     * 查询热门帖子
     * @return
     */
    public  List<Post> selecthotpost();
    /**
     * 通过用户Id查询该用户发表的所有博客 默认降序
     * @return
     */
    List<Post> postSelectListByUserId(String userId);
}
