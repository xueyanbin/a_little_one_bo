package com.list.fans.model;

public class Photo {
    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public String getPhotoPostId() {
        return photoPostId;
    }

    public void setPhotoPostId(String photoPostId) {
        this.photoPostId = photoPostId;
    }

    public String getPhotoUserId() {
        return photoUserId;
    }

    public void setPhotoUserId(String photoUserId) {
        this.photoUserId = photoUserId;
    }

    public String getPhotoDateTime() {
        return photoDateTime;
    }

    public void setPhotoDateTime(String photoDateTime) {
        this.photoDateTime = photoDateTime;
    }

    public String getPhotoStatus() {
        return photoStatus;
    }

    public void setPhotoStatus(String photoStatus) {
        this.photoStatus = photoStatus;
    }

    private String photoId;
    private String photoName;
    private String photoPostId;
    private String photoUserId;
    private String photoDateTime;
    private String photoStatus;
}
