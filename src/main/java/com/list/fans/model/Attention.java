package com.list.fans.model;

import java.util.List;

public class Attention {
    public String getAttentionId() {
        return attentionId;
    }

    public void setAttentionId(String attentionId) {
        this.attentionId = attentionId;
    }

    public String getAttentionUserID() {
        return attentionUserID;
    }

    public void setAttentionUserID(String attentionUserID) {
        this.attentionUserID = attentionUserID;
    }

    public String getAttentionPassiveUserId() {
        return attentionPassiveUserId;
    }

    public void setAttentionPassiveUserId(String attentionPassiveUserId) {
        this.attentionPassiveUserId = attentionPassiveUserId;
    }

    public String getAttentionDateTime() {
        return attentionDateTime;
    }

    public void setAttentionDateTime(String attentionDateTime) {
        this.attentionDateTime = attentionDateTime;
    }

    public String getAttentionStatus() {
        return attentionStatus;
    }

    public void setAttentionStatus(String attentionStatus) {
        this.attentionStatus = attentionStatus;
    }

    private String attentionId;
    private String attentionUserID;
    private String attentionPassiveUserId;
    private String attentionDateTime;
    private String attentionStatus;

    List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
