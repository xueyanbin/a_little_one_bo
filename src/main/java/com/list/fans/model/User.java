package com.list.fans.model;



public class User {
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex;
    }

    public String getUserBirth() {
        return userBirth;
    }

    public void setUserBirth(String userBirth) {
        this.userBirth = userBirth;
    }

    public String getUserHeadshot() {
        return userHeadshot;
    }

    public void setUserHeadshot(String userHeadshot) {
        this.userHeadshot = userHeadshot;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserTelephone() {
        return userTelephone;
    }

    public void setUserTelephone(String userTelephone) {
        this.userTelephone = userTelephone;
    }

    public String getUserMotto() {
        return userMotto;
    }

    public void setUserMotto(String userMotto) {
        this.userMotto = userMotto;
    }

    public String getUserDateTime() {
        return userDateTime;
    }

    public void setUserDateTime(String userDateTime) {
        this.userDateTime = userDateTime;
    }

    public String getUserStatue() {
        return userStatue;
    }

    public void setUserStatue(String userStatue) {
        this.userStatue = userStatue;
    }

    public String getUserAttentionCount() {
        return userAttentionCount;
    }

    public void setUserAttentionCount(String userAttentionCount) {
        this.userAttentionCount = userAttentionCount;
    }

    public String getUserFansCount() {
        return userFansCount;
    }

    public void setUserFansCount(String userFansCount) {
        this.userFansCount = userFansCount;
    }

    private String userId;
    private String userName;
    private String userPassword;
    private String userSex;
    private String userBirth;
    private String userHeadshot;
    private String userEmail;
    private String userTelephone;
    private String userMotto;
    private String userDateTime;
    private String userStatue;
    private String userAttentionCount;
    private String userFansCount;
}