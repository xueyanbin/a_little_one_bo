package com.list.fans.model;

public class Transpond {
    private String transpondId;
    private String transpondContent;
    private String transpondPostId;
    private String transpondUserId;
    private String transpondCount;
    private String transpondLikeCount;
    private String transpondCategoryId;
    private String transpondDateTime;
    private String transpondStatus;

    public String getTranspondCount() {
        return transpondCount;
    }

    public void setTranspondCount(String transpondCount) {
        this.transpondCount = transpondCount;
    }

    public String getTranspondLikeCount() {
        return transpondLikeCount;
    }

    public void setTranspondLikeCount(String transpondLikeCount) {
        this.transpondLikeCount = transpondLikeCount;
    }

    public String getTranspondCategoryId() {
        return transpondCategoryId;
    }

    public void setTranspondCategoryId(String transpondCategoryId) {
        this.transpondCategoryId = transpondCategoryId;
    }

    public String getTranspondId() {
        return transpondId;
    }

    public void setTranspondId(String transpondId) {
        this.transpondId = transpondId;
    }

    public String getTranspondContent() {
        return transpondContent;
    }

    public void setTranspondContent(String transpondContent) {
        this.transpondContent = transpondContent;
    }

    public String getTranspondPostId() {
        return transpondPostId;
    }

    public void setTranspondPostId(String transpondPostId) {
        this.transpondPostId = transpondPostId;
    }

    public String getTranspondUserId() {
        return transpondUserId;
    }

    public void setTranspondUserId(String transpondUserId) {
        this.transpondUserId = transpondUserId;
    }

    public String getTranspondDateTime() {
        return transpondDateTime;
    }

    public void setTranspondDateTime(String transpondDateTime) {
        this.transpondDateTime = transpondDateTime;
    }

    public String getTranspondStatus() {
        return transpondStatus;
    }

    public void setTranspondStatus(String transpondStatus) {
        this.transpondStatus = transpondStatus;
    }

}
