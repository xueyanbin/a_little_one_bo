package com.list.fans.model;

public class Comment {


    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getCommentUserId() {
        return commentUserId;
    }

    public void setCommentUserId(String commentUserId) {
        this.commentUserId = commentUserId;
    }

    public String getCommentPostId() {
        return commentPostId;
    }

    public void setCommentPostId(String commentPostId) {
        this.commentPostId = commentPostId;
    }

    public String getCommentDateTime() {
        return commentDateTime;
    }

    public void setCommentDateTime(String commentDateTime) {
        this.commentDateTime = commentDateTime;
    }

    public String getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public int getCommentTranspondCount() {
        return commentTranspondCount;
    }

    public void setCommentTranspondCount(int commentTranspondCount) {
        this.commentTranspondCount = commentTranspondCount;
    }

    public int getCommentLikeCount() {
        return commentLikeCount;
    }

    public void setCommentLikeCount(int commentLikeCount) {
        this.commentLikeCount = commentLikeCount;
    }

    public String getCommentTranspondId() {
        return commentTranspondId;
    }

    public void setCommentTranspondId(String commentTranspondId) {
        this.commentTranspondId = commentTranspondId;
    }

    private String commentId;
    private String commentContent;
    private int commentTranspondCount;
    private int commentLikeCount;
    private String commentUserId;
    private String commentPostId;
    private String commentDateTime;
    private String commentStatus;
    private String commentTranspondId;

}
