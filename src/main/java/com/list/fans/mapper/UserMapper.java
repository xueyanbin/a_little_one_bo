package com.list.fans.mapper;

import com.list.fans.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    /**
     * 用户登录
     * @param account  账号（手机号/邮箱）
     * @param userPassword  密码
     * @return
     */
    User userLogin(@Param("account") String account,@Param("userPassword") String userPassword);

    /**
     * 修改密码
     * @param userId
     * @param userPassword
     * @return
     */
    int updateUserPassword(@Param("userId") String userId,@Param("userPassword") String userPassword);

    /**
     * 查询用户信息
     * @param userId
     * @return
     */
    User selectUserByUserId(@Param("userId") String userId);

    /**
     * 查询用户名是否重复
     * @param userName
     * @return
     */
    User selectUserNameIsOk(@Param("userName") String userName);
    /**
     * 查询用户电话是否重复
     * @param userTelephone
     * @return
     */
    User selectUserTelephoneIsOk(@Param("userTelephone") String userTelephone);
    /**
     * 查询用户邮箱是否重复
     * @param userEmail
     * @return
     */
    User selectUserEmailIsOk(@Param("userEmail") String userEmail);

    /**
     * 注册用户
     * @param user
     * @return
     */
    int insertUser(User user);



    /**
     * 根据用户id查询用户信息@xyb
     * @param userid
     * @return
     */
    public User selectUserMessage(@Param("userid")String userid);

    /**
     * 根据用户id修改用户头像@xyb
     * @param photo
     * @param userid
     */
    public void updateUserPhoto(@Param("photo") String photo,@Param("userid") String userid);

    /**
     * 根据用户id修改用户密码@xyb
     * @param password
     * @param userid
     */
    //public  void updateUserPassword(@Param("password") String password,@Param("userid") String userid);

    /**
     * 根据用户id修改用户信息@xyb
     *
     */
    public  void updateUserMessage(User user);

    /**
     * 根据用户id查询用户的粉丝数量
     * @param userid
     * @return
     */
    public  Integer selectfans(@Param("userid")String userid);


    /**
     * 用户登录
     * @param userTelephone  手机号
     * @return
     */
    User userLoginByCode(@Param("userTelephone") String userTelephone);
}
