package com.list.fans.mapper;

import com.list.fans.model.Attention;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttentionMapper {
    /**
     * 查询我的关注：我关注的用户
     * @param userId
     * @return
     */
    List<Attention> selectAttentionListByUserIdWithUsers(String userId);

    /**
     * 添加我的关注
     * @param attention
     */
    void insertAttentionUser (Attention attention);

    /**
     * 删除我的关注
     * @param attentionUserID
     * @param attentionPassiveUserId
     */
    void deletetAttentionUser(@Param("attentionUserID") String attentionUserID, @Param("attentionPassiveUserId") String attentionPassiveUserId);
}
