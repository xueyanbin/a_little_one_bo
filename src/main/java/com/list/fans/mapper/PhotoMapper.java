package com.list.fans.mapper;

import com.list.fans.model.Photo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhotoMapper {
    /**
     *
     * 根据用户id查询相册
     * @param userid
     * @return
     */
    public List<Photo> selectPhoto(@Param("userid") String userid);

    /**
            * 插入图片
     * @param photo
     */
    void insertPhoto(Photo photo);
}
