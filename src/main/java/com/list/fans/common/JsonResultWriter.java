package com.list.fans.common;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class JsonResultWriter{
	public static void write(HttpServletResponse response , Object object) throws ServletException, IOException{
		
		response.setContentType("application/json");
		response.setHeader("Content-Type", "text/html");
		Gson gson =new Gson();
		String json=gson.toJson(object);
		PrintWriter writer= response.getWriter();
		writer.print(json);
		writer.close();
	
	}
	
}
