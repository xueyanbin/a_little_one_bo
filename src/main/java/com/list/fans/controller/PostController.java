package com.list.fans.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.list.fans.common.Constant;
import com.list.fans.common.JsonResult;
import com.list.fans.model.Photo;
import com.list.fans.model.Post;
import com.list.fans.model.Praise;
import com.list.fans.model.Transpond;
import com.list.fans.service.PhotoService;
import com.list.fans.service.PostService;
import com.list.fans.service.PraiseService;
import com.list.fans.service.TranspondService;
import com.list.fans.util.UidGenerators;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class PostController {
    @Autowired
    PostService postService;
    @Autowired
    PraiseService praiseService;
    @Autowired
    TranspondService transpondService;
    @Autowired
    PhotoService photoService;
    private static Logger logger = LoggerFactory.getLogger(PostController.class);

    /**
     * 查询所有贴子
     * 传入参数 pageNum 当前页数   pageNum 每页显示数
     * 传出参数 Post的所有字段和Photo的所有字段
     * 接口 /post/select/all
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/post/select/all")
    @ResponseBody
    public Object PostSelectAll(@RequestParam("pageNum") Integer pageNum,
                                    @RequestParam("pageSize") Integer pageSize){
        JsonResult result = null;
        try {
            PageHelper.startPage(pageNum,pageSize);
            List<Post> postList= postService.postSelectAll();
            PageInfo<Post> pageInfo =new PageInfo<>(postList,pageSize);
            logger.info("传入参数{}");
            result = new JsonResult("帖子查询成功",Constant.STATUC_SUCCESS,postList);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("帖子查询异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 插入帖子
     * 传入参数
     * postContent帖子内容
     * postUserId 用户id
     * photoName图片名称
     * 接口 /post/insert
     * 没有返回值
     * @param post
     * @return
     */
    @RequestMapping("/post/insert")
    @ResponseBody
    public Object insertPost( Post post,Photo photo){
        JsonResult result = null;
        try {
            String id=UidGenerators.getUuid();
            post.setPostId(id);
            photo.setPhotoPostId(id);
            logger.info("传入参数{}",post);
            postService.insertPost(post);
            photoService.insertPhoto(photo);
            result = new JsonResult("帖子插入询成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("帖子插入异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 删除帖子
     * 把帖子的status改0
     * 传入参数 postId
     * 接口/post/delete
     * 没有返回值
     * @param postId
     * @return
     */
    @RequestMapping("/post/delete")
    @ResponseBody
    public Object deletePost( String postId){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",postId);
            postService.deletePost(postId);
            result = new JsonResult("帖子删除成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("帖子删除异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 点赞 更新POST_LIKE_COUNT+1
     * 点赞表插入数据
     * 传入参数 postId 帖子id
     * user 用户id
     * 接口/up/post/praise
     * 没有返回值
     * @param postId
     * @return
     */
    @RequestMapping("/up/post/praise")
    @ResponseBody
    public Object updatePostPraiseUp( String postId,String userId){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",postId);
            postService.updatePostPraiseUp(postId);
            Praise praise=new Praise();
            praise.setPraiseUserId(userId);
            praise.setPraisePostId(postId);
            praiseService.insertPraiseByPost(praise);
            result = new JsonResult("帖子点赞成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("帖子点赞异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 取消赞 更新POST_LIKE_COUNT-1
     * 点赞表删除数据
     * 传入参数 postId
     * 接口/down/post/praise
     * 没有返回值
     * @param postId
     * @return
     */
    @RequestMapping("/down/post/praise")
    @ResponseBody
    public Object updatePostPraiseDown(
            String postId,
           String praiseUserId){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",postId);
            postService.updatePostPraiseDown(postId);
            Praise praise=new Praise();
            praise.setPraisePostId(postId);
            praise.setPraiseUserId(praiseUserId);
            praiseService.deletePraiseByPost(praise);
            result = new JsonResult("帖子取消赞成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("帖子取消赞异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 转发
     * 传入参数
     * transpondContent 转发写的评论内容
     * transpondPostId 转发贴的id  transpondUserId  用户id
     * transpondCategoryId  类别id
     * 接口/update/post/transpond
     * 没有返回值
     * @param transpond
     * @return
     */
    @RequestMapping("/update/post/transpond")
    @ResponseBody
    public Object updatePostTranUp(Transpond transpond){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",transpond);
            transpondService.insertTrans(transpond);
            postService.updatePostTranUp(transpond.getTranspondPostId());
            result = new JsonResult("帖子转发成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("帖子转发异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 删除转发
     * 传入参数 postId 帖子Id
     * transpondId转发Id
     * 接口/delete/transpond/byPostId
     * 无返回值
     * @param postId
     * @param transpondId
     * @return
     */
    @RequestMapping("/delete/transpond/byPostId")
    @ResponseBody
    public Object deleteTransByPost(String postId,String transpondId){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",postId,transpondId);
            transpondService.deleteTrans(transpondId);
            postService.deletePostTranDown(postId);
            result = new JsonResult("转贴删除成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("转贴删除异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 根据帖子内容迷糊搜素帖子@xyb
     * 接口/select/post/mohu
     * 传入参数posttent\pageNum\pageSize
     * 请求类型Post
     * @param posttent
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/select/post/mohu",method = RequestMethod.POST)
    @ResponseBody
    public Object  selectPostmohu(
            @RequestParam(value = "posttent",defaultValue = "",required = false)String  posttent,
            @RequestParam(value = "pageNum",defaultValue = "1",required = false) Integer pageNum,
            @RequestParam(value = "pageSize") Integer pageSize
    ){
        JsonResult result=null;
        try{
            PageHelper.startPage(pageNum,pageSize);
            List<Post> list=postService.selectPostmohu(posttent);
            PageInfo<Post> pageInfo =new PageInfo<>(list,pageSize);
            logger.info("参数是{}",posttent);
            result=new JsonResult("查询成功","200",list);
        }catch (Exception ex){
            logger.info(ex.getMessage());
            result=new JsonResult("出错了","500");
        }
        return  result;
    }

    /**
     * 热门帖子
     * 接口/select/post/hot
     * 传入参数pageNum、PageSize
     * 请求类型Post
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/select/post/hot",method = RequestMethod.POST)
    @ResponseBody
    public  Object selecthotpost(
            @RequestParam(value = "pageNum",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(value = "pageSize",defaultValue ="",required = false)Integer pageSize
    ){
        JsonResult result=null;
        try {
            logger.info("参数：{}{}",pageNum,pageSize);
            PageHelper.startPage(pageNum,pageSize);
            List<Post> list=postService.selecthotpost();
            PageInfo<Post> pageInfo=new PageInfo<>(list,pageSize);
            result=new JsonResult("查询成功","200",list);
        }catch (Exception ex){
            logger.info(ex.getMessage());
            result=new JsonResult("出错了","500");
        }
        return result;
    }

    /**
     * 通过用户Id查询该用户发表的所有博客 默认降序
     * 传入参数：userId 当前用户Id
     * @param userId
     * @return
     */
    @RequestMapping("/post/select/list/by/userId")
    @ResponseBody
    public Object postSelectListByUserId(
            @RequestParam(value = "userId",required = false,defaultValue = "") String userId
    ){
        JsonResult result = null;
        try {
            List<Post> postList = postService.postSelectListByUserId(userId);
            if(postList.size()>0){
                result = new JsonResult("查询成功",Constant.STATUC_SUCCESS,postList);
            }else {
                result = new JsonResult("查询失败",Constant.STATUC_UNFOUND);
            }
        }catch (Exception ex){
            result = new JsonResult("查询异常",Constant.STATUC_FAILURE);
        }
        return result;
    }
}
