package com.list.fans.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.list.fans.common.Constant;
import com.list.fans.common.JsonResult;
import com.list.fans.model.Attention;
import com.list.fans.service.AttentionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class AttentionController {
    private static Logger logger = LoggerFactory.getLogger(PostController.class);

    @Autowired
    AttentionService attentionService;

    /**
     * 查询我的关注：我关注的用户
     * @param pageNum  当前页数
     * @param pageSize 当前页记录
     * @param userId 用户Id
     * @return
     */
    @RequestMapping("/select/attention/list/by/userId/with/users")
    @ResponseBody
    public Object selectAttentionListByUserIdWithUsers(@RequestParam("pageNum") Integer pageNum,
                                                       @RequestParam("pageSize") Integer pageSize,
                                                       @RequestParam("userId") String userId
    ){
        JsonResult result = null;
        try {
            PageHelper.startPage(pageNum,pageSize);
            List<Attention> attentionList = attentionService.selectAttentionListByUserIdWithUsers(userId);
            PageInfo<Attention> pageInfo =new PageInfo<>(attentionList,pageSize);
            logger.info("用户Id：{},当前页数：{},当前页记录：{}",userId,pageNum,pageSize);
            if (attentionList.size()>0){
                result = new JsonResult("查询成功",Constant.STATUC_SUCCESS,pageInfo);
            }else {
                result = new JsonResult("查询失败",Constant.STATUC_UNFOUND);
            }
        }catch (Exception ex){
            result = new JsonResult("查询异常", Constant.STATUC_FAILURE);
        }
        return result;
    }

    /**
     *
     * 添加我的关注
     * 传入参数 attentionUserID 关注用户id   attentionPassiveUserId  被关注用户id
     * 接口/insert/attention/user
     * 没有返回值
     * @param attention
     * @return
     */
    @RequestMapping("/insert/attention/user")
    @ResponseBody
    public Object insertAttentionUser(Attention attention ){
        JsonResult result = null;
        try {
            logger.info("用户{}",attention);
          attentionService.insertAttentionUser(attention);
            result = new JsonResult("添加成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            result = new JsonResult("添加异常", Constant.STATUC_FAILURE);
        }
        return result;
    }

    /**
     *
     * 删除我的关注
     * 传入参数 attentionUserID 关注用户id   attentionPassiveUserId  被关注用户id
     * 没有返回值
     * 接口 /delete/attention/user
     * @param attentionUserID
     * @param attentionPassiveUserId
     * @return
     */
    @RequestMapping("/delete/attention/user")
    @ResponseBody
    public Object deletetAttentionUser(String attentionUserID,String attentionPassiveUserId ){
        JsonResult result = null;
        try {
            attentionService.deletetAttentionUser(attentionUserID,attentionPassiveUserId);
            logger.info("用户Id：{},被关注用户Id{}",attentionUserID,attentionPassiveUserId);
            result = new JsonResult("删除成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            result = new JsonResult("删除异常", Constant.STATUC_FAILURE);
        }
        return result;
    }
}
