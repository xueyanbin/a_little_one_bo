package com.list.fans.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.list.fans.common.Constant;
import com.list.fans.common.JsonResult;
import com.list.fans.model.Collect;
import com.list.fans.service.CollectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class CollectController {
    @Autowired
    CollectService collectService;
    private static Logger logger = LoggerFactory.getLogger(CollectController.class);

    /**
     * 收藏
     * 传入参数
     * collectUserId 用户id collectPostId 帖子Id
     * 接口/update/post/transpond
     * 没有返回值
     * @param collect
     * @return
     */
    @RequestMapping("/insert/post/collect")
    @ResponseBody
    public Object insertCollect(Collect collect){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",collect);
            collectService.insertCollect(collect);
            result = new JsonResult("帖子收藏成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("帖子收藏异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 收藏
     * 传入参数
     * collectUserId 用户id collectPostId 帖子Id
     * 接口/update/post/transpond
     * 没有返回值
     * @param collectId
     * @return
     */
    @RequestMapping("/delete/post/collect")
    @ResponseBody
    public Object insertCollect(String collectId){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",collectId);
            collectService.deleteCollect(collectId);
            result = new JsonResult("帖子收藏删除成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("帖子收藏删除异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 查询用户收藏的帖子
     * 接口/select/collect/byuserid
     * 传入的参数userid、pageNum、pageSize
     * 请求类型post
     * @param userid
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/select/collect/byuserid",method = RequestMethod.POST)
    @ResponseBody
    public  Object selectcollectpost(
            @RequestParam(value = "userid",defaultValue = "",required = false)String userid,
            @RequestParam(value = "pageNume",defaultValue = "1",required = false)Integer pageNum,
            @RequestParam(value = "pageSize",defaultValue = "",required = false)Integer pageSize
    )
    {
        JsonResult result=null;
        try {
            logger.info("参数：{}{}{}",userid,pageNum,pageSize);
            PageHelper.startPage(pageNum,pageSize);
            List<Collect> list=collectService.selectcollectpost(userid);
            PageInfo<Collect> pageInfo=new PageInfo<>(list,pageSize);
            result=new JsonResult("查询成功","200",list);
        }catch (Exception ex){
            logger.info(ex.getMessage());
            result=new JsonResult("出错了","500");
        }
        return result;
    }
}
