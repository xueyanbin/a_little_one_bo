package com.list.fans.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.list.fans.common.JsonResult;
import com.list.fans.model.Praise;
import com.list.fans.service.PraiseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class PraiseController {
    @Autowired
    PraiseService praiseService;
    private static Logger logger = LoggerFactory.getLogger(PraiseController.class);

    /**
     * 根据用户id查询我点过赞的帖子
     * 接口/select/praise/byuserid
     * 传入参数userid、pageNum、pageSize
     * 请求类型Post
     * @param userid
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/select/praise/byuserid",method = RequestMethod.POST)
    @ResponseBody
    public Object selectpraisepost(@RequestParam(value = "userid",defaultValue = "",required = false)String userid,
                                   @RequestParam(value = "pageNum",defaultValue = "1",required = false)Integer pageNum,
                                   @RequestParam(value = "pageSize")Integer pageSize){
        JsonResult result;
        try{
            logger.info("参数：{}{}",pageNum,pageSize);
            PageHelper.startPage(pageNum,pageSize);
            List<Praise> list=praiseService.selectpraisepost(userid);
            PageInfo<Praise> pageInfo=new PageInfo<>(list,pageSize);
            result=new JsonResult("查询成功","200",list);
        }catch (Exception ex){
            logger.info(ex.getMessage());
            result=new JsonResult("出错了","500");
        }
        return result;
    }

}
