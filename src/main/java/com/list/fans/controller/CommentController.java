package com.list.fans.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.list.fans.common.Constant;
import com.list.fans.common.JsonResult;
import com.list.fans.model.Comment;
import com.list.fans.model.Post;
import com.list.fans.service.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class CommentController {
    @Autowired
    CommentService commentService;
    private static Logger logger = LoggerFactory.getLogger(CommentController.class);

    /**
     *传入参数 commentCount评论内容
     * commentUserId 用户id
     * （commentPostId帖子Id  commentTranspondId转发贴的id）二选一
     * 接口/insert/comment
     * 无返回值
     * @param comment
     * @return
     */
    @RequestMapping("/insert/comment")
    @ResponseBody
    public Object insertComment(Comment comment){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",comment);
            commentService.insertComment(comment);
            result = new JsonResult("评论帖子成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("评论帖子异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     *传入参数   commentId 评论id
     * 接口/delete/comment
     * 无返回值
     * @param commentId
     * @return
     */
    @RequestMapping("/delete/comment")
    @ResponseBody
    public Object deleteComment(String commentId){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",commentId);
            commentService.deleteComment(commentId);
            result = new JsonResult("删除评论成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("删除评论异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 根据用户id查询评论@xyb
     * 传入参数userid\pageNum\pageSize
     * 接口/select/comment/message
     * 请求类型Post
     *
     * @param userid
     * @return
     * @Param pageNum
     * @Parrm pageSize
     */
    @RequestMapping(value = "/select/comment/message", method = RequestMethod.POST)
    @ResponseBody
    public JsonResult selectComment(
            @RequestParam(value = "userid", defaultValue = "", required = false) String userid,
            @RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
            @RequestParam(value = "pageSize") Integer pageSize
    ) {
        JsonResult result = null;
        try {
            logger.info("参数：{}", userid);
            PageHelper.startPage(pageNum, pageSize);
            List<Comment> list = commentService.selectComment(userid);
            PageInfo<Comment> pageInfo = new PageInfo<>(list, pageSize);
            result = new JsonResult("查询成功", "200", list);
        } catch (Exception ex) {
            logger.info(ex.getMessage());
            result = new JsonResult("出错了", "500");
        }
        return result;
    }

    /**
     * 根据帖子id查询帖子评论
     * 接口/select/comment/bypostid
     * 传入参数postid、pageNum、pageSize
     * @param postid
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping(value = "/select/comment/bypostid",method = RequestMethod.POST)
    @ResponseBody
    public Object selectcommentbypostid(
            @RequestParam(value = "postid", defaultValue = "", required = false) String postid,
            @RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
            @RequestParam(value = "pageSize") Integer pageSize
    ) {
        JsonResult result = null;
        try {
            logger.info("参数：{}{}{}",pageNum,pageSize,postid);
            PageHelper.startPage(pageNum,pageSize);
            List<Comment> list=commentService.selectcommentbypostid(postid);
            PageInfo<Comment> pageInfo=new PageInfo<>(list,pageSize);
            result=new JsonResult("查询成功","200",list);
        } catch (Exception ex) {
            logger.info(ex.getMessage());
            result=new JsonResult("出错了","500");
        }
        return result;
    }
}
