package com.list.fans.controller;

import com.list.fans.common.Constant;
import com.list.fans.common.JsonResult;
import com.list.fans.model.Post;
import com.list.fans.model.Praise;
import com.list.fans.model.Transpond;
import com.list.fans.service.PraiseService;
import com.list.fans.service.TranspondService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TranspondController {
    @Autowired
    TranspondService transpondService;
    @Autowired
    PraiseService praiseService;
    private static Logger logger = LoggerFactory.getLogger(TranspondController.class);

    /**
     * 对转发贴点赞
     * 传入参数
     * transpondId转发贴id
     * 接口 /update/transpond/praiseUp
     * 没有返回值
     * @param transpondId
     * @return
     */
    @RequestMapping("/update/transpond/praiseUp")
    @ResponseBody
    public Object updateTransPraiseUp(String transpondId){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",transpondId);
            transpondService.updateTransPraiseUp(transpondId);
            result = new JsonResult("点赞成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("点赞异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 取消转发贴的赞
     * 传入参数  userId用户id
     *  transpondId转发贴id
     * 接口 /update/transpond/praiseUp
     * 没有返回值
     * @param userId
     * @param transpondId
     * @return
     */
    @RequestMapping("/update/transpond/praiseDown")
    @ResponseBody
    public Object updateTransPraiseDown(String userId,String transpondId){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",transpondId);
            transpondService.updateTransPraiseDown(transpondId);
            result = new JsonResult("取消点赞成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("取消点赞异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 转发
     * 传入参数
     * transpondContent 转发写的评论内容
     * transpondPostId 转发贴的id  transpondUserId  用户id
     * 接口/update/post/transpond
     * 没有返回值
     * @param transpondId
     * @param transpond
     * @return
     */
    @RequestMapping("/insert/transpond")
    @ResponseBody
    public Object updatePostTranUp(Transpond transpond,String transpondId){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",transpond);
            transpondService.insertTrans(transpond);
            transpondService.updateTransCountUp(transpondId);
            result = new JsonResult("帖子转发成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("帖子转发异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }

    /**
     * 删除转发
     * 传入参数 transpondId 转发后的帖子id
     * 接口/delete/transpond/byPostId
     * 无返回值
     * @param transpondId
     * @return
     */
    @RequestMapping("/delete/transpond")
    @ResponseBody
    public Object deleteTransCountDown(String transpondId){
        JsonResult result = null;
        try {
            logger.info("传入参数{}",transpondId);
            transpondService.deleteTrans(transpondId);
            result = new JsonResult("转贴删除成功",Constant.STATUC_SUCCESS);
        }catch (Exception ex){
            logger.info("错误信息{}",ex.getMessage());
            result = new JsonResult("转贴删除异常",Constant.STATUC_FAILURE);
        }
        return  result;
    }
}
